﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alimevo2.Models
{
    public class Article
    {
        public int Id;
        public string Title;
        public string Picture;
        public string Body;
        public DateTime DateOfCreation;
        public DateTime DateOfModification;
        public int FK_cook_user;
    }
}
